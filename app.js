"use strict";
// Simple generic
function echo(data) {
    return data;
}
console.log(echo("Max"));
console.log(echo("Max").length);
console.log(echo(27));
console.log(echo({ name: "Max", age: 27 }));
// Better Generic
function betterEcho(data) {
    return data;
}
console.log(betterEcho("Max").length);
//console.log(betterEcho<number>("27")/*.length*/);
console.log(betterEcho(27));
console.log(betterEcho({ name: "Max", age: 27 }));
// Built-in generics
var testResults = [1.94, 2.33];
testResults.push(-2.99);
//testResults.push("5");
// Arrays
function printAll(args) {
    args.forEach(function (element) { return console.log(element); });
}
printAll(["Apples", "Bananas"]);
// Generic types
var echo2 = betterEcho;
console.log(echo2("Something"));
// Generic classes
var SimpleMath = /** @class */ (function () {
    function SimpleMath() {
    }
    SimpleMath.prototype.calculate = function () {
        return +this.baseValue * +this.multiplyValue;
    };
    return SimpleMath;
}());
var simpleMath = new SimpleMath();
simpleMath.baseValue = "10";
simpleMath.multiplyValue = 20;
console.log(simpleMath.calculate());
